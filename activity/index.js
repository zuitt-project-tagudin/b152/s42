let inputFirstName = document.querySelector("#txt-first-name");
let inputLastName = document.querySelector("#txt-last-name");
let fullNameDisplay = document.querySelector("#full-name-display");
let submitButton = document.querySelector("#submit-btn");



const showName = () => {

	fullNameDisplay.innerHTML = `${inputFirstName.value}  ${inputLastName.value}`

}


const reset = () => {
	inputFirstName.value = "";
	inputLastName.value = "";
	fullNameDisplay.innerHTML = "";
}


const submit = () => {

	if (inputFirstName.value !== "" &&
		inputLastName.value !== "") {

		alert(`Thank you for registering, ${fullNameDisplay.innerHTML}!`)

		reset();

		window.location.href = "https://zuitt.co";

	} else {
		alert("Please input first and last name")
	}

}


inputFirstName.addEventListener('keyup', showName);
inputLastName.addEventListener('keyup', showName);

submitButton.addEventListener("click", submit);




